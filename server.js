//servidor
const express = require('express');
//manipular arquivos
const fs = require('fs');
const app = express();
//porta onde o servidor está
const porta = 3000;
const jwt = require('jsonwebtoken');
const SECRET = "SohEuSei"

app.listen(porta);
app.use(express.static('public'))
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/view/index.html');
})

//retorna os dados coletados na pesquisa
app.get('/estatisticas', function (req, res) {
    fs.readFile('dados/pesquisa.csv', 'utf8', function (err, data) {
        if (err) {
            console.log("Erro ao ler arquivo: " + err);
        } else {
            var votosEnviados = data.split("\n")

            let votos = []

            for (let i = 0; i < votosEnviados.length - 1; i++) {
                const element = votosEnviados[i];
                votos.push(element.split(","))
            }

            let total = votos.length;

            var qualidadeAtendimento = [0, 0, 0]
            var notaAtendimento = [0, 0, 0, 0, 0]
            var comportamentoAtendente = [0, 0, 0, 0, 0]
            var rangeProduto = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            var idades = [0, 0, 0, 0, 0]

            for (let i = 0; i < votos.length; i++) {
                switch (votos[i][0]) {
                    case "0":
                        qualidadeAtendimento[0]++;
                        break;
                    case "1":
                        qualidadeAtendimento[1]++;
                        break;
                    case "2":
                        qualidadeAtendimento[2]++;
                        break;
                }
                switch (votos[i][1]) {
                    case "0":
                        notaAtendimento[0]++;
                        break;
                    case "1":
                        notaAtendimento[1]++;
                        break;
                    case "2":
                        notaAtendimento[2]++;
                        break;
                    case "3":
                        notaAtendimento[3]++;
                        break;
                    case "4":
                        notaAtendimento[4]++;
                        break;
                }
                switch (votos[i][2]) {
                    case "0":
                        comportamentoAtendente[0]++;
                        break;
                    case "1":
                        comportamentoAtendente[1]++;
                        break;
                    case "2":
                        comportamentoAtendente[2]++;
                        break;
                    case "3":
                        comportamentoAtendente[3]++;
                        break;
                    case "4":
                        comportamentoAtendente[4]++;
                        break;
                }
                switch (votos[i][3]) {
                    case "0":
                        rangeProduto[0]++;
                        break;
                    case "1":
                        rangeProduto[1]++;
                        break;
                    case "2":
                        rangeProduto[2]++;
                        break;
                    case "3":
                        rangeProduto[3]++;
                        break;
                    case "4":
                        rangeProduto[4]++;
                        break;
                    case "5":
                        rangeProduto[5]++;
                        break;
                    case "6":
                        rangeProduto[6]++;
                        break;
                    case "7":
                        rangeProduto[7]++;
                        break;
                    case "8":
                        rangeProduto[8]++;
                        break;
                    case "9":
                        rangeProduto[9]++;
                        break;
                }
                let opcao;
                if (votos[i][5] < 15) {
                    opcao = 0;
                } else if (votos[i][5] >= 15 && votos[i][5] <= 21) {
                    opcao = 1;
                } else if (votos[i][5] > 21 && votos[i][5] <= 35) {
                    opcao = 2
                } else if (votos[i][5] > 35 && votos[i][5] <= 50) {
                    opcao = 3
                } else if (votos[i][5] > 50) {
                    opcao = 4
                }
                switch (opcao) {
                    case 0:
                        idades[0]++;
                        break;
                    case 1:
                        idades[1]++;
                        break;
                    case 2:
                        idades[2]++;
                        break;
                    case 3:
                        idades[3]++;
                        break;
                    case 4:
                        idades[4]++;
                        break;
                }
            }
            let resposta = {
                "a": {
                    "nãoFoiAtendida": percentual(qualidadeAtendimento[0], total),
                    "parcialmenteAtendida": percentual(qualidadeAtendimento[1], total),
                    "totalmenteAtendida": percentual(qualidadeAtendimento[2], total)
                },
                "b": {
                    "péssimo": percentual(notaAtendimento[0], total),
                    "ruim": percentual(notaAtendimento[1], total),
                    "aceitável": percentual(notaAtendimento[2], total),
                    "bom": percentual(notaAtendimento[3], total),
                    "excelente": percentual(notaAtendimento[4], total)
                },
                "c": {
                    "indelicado": percentual(comportamentoAtendente[0], total),
                    "malHumorado": percentual(comportamentoAtendente[1], total),
                    "neutro": percentual(comportamentoAtendente[2], total),
                    "educado": percentual(comportamentoAtendente[3], total),
                    "muitoAtencioso": percentual(comportamentoAtendente[4], total)
                },
                "d": {
                    "zero": percentual(rangeProduto[0], total),
                    "um": percentual(rangeProduto[1], total),
                    "dois": percentual(rangeProduto[2], total),
                    "tres": percentual(rangeProduto[3], total),
                    "quatro": percentual(rangeProduto[4], total),
                    "cinco": percentual(rangeProduto[5], total),
                    "seis": percentual(rangeProduto[6], total),
                    "sete": percentual(rangeProduto[7], total),
                    "oito": percentual(rangeProduto[8], total),
                    "nove": percentual(rangeProduto[9], total),
                    "dez": percentual(rangeProduto[10], total)
                },
                "e": {
                    "menosDe15": percentual(idades[0], total),
                    "entre15e21": percentual(idades[1], total),
                    "entre22e35": percentual(idades[2], total),
                    "entre36e50": percentual(idades[3], total),
                    "acima50": percentual(idades[4], total)
                }
            }
            res.send(resposta)
        }

    })

})

app.get('/usuarios', function (req, res) {
    fs.readFile('dados/usuarios.csv', 'utf8', function (err, data) {
        if (err) {
            console.log("Erro ao ler arquivo: " + err);
        } else {
            var usuariosCadastrados = data.split("\n")

            let usuarios = []

            for (let i = 0; i < usuariosCadastrados.length - 1; i++) {
                const element = usuariosCadastrados[i];
                usuarios.push(element.split(","))
            }
            res.send(usuarios)
        }
    })
})


function percentual(valor, total) {
    return (valor / total) * 100
}

//adiciona novos votos captados do formulario
app.post('/pesquisa', verificaUser, function (req, res) {
    let voto = `${req.body.nmAtendida},${req.body.nmNota},${req.body.nmNota},${req.body.nmComportamento},${req.body.nmProduto},${req.body.nmIdade},${req.body.nmGenero},${req.body.nmTimeStamp}`
    msn = {}
    fs.appendFile("dados/pesquisa.csv", `${voto},\n`, function (err) {
        if (err) {
            msn.status = 500
            msn.mensagem = "err"
            throw err
        } else {
            msn.status = 200
            msn.mensagem = "Pesquisa Registrada Com sucesso"
            console.log("Pesquisa Registrada Com sucesso");
        }
        res.json(msn)
    })
})

app.get('/relatorio',function (req, res) {
    res.sendFile(__dirname + '/view/relatorio.html')
})

app.get('/login', function (req, res) {
    res.sendFile(__dirname + '/view/login.html')
})

app.get('/cadastro', function (req, res) {
    res.sendFile(__dirname + '/view/cadastro.html')
})

app.post('/cadastro', function (req, res) {
    let cadastro = `${req.body.nmUsuario},${req.body.nmSenha}`
    msn = {}
    fs.appendFile("dados/usuarios.csv", `${cadastro},\n`, function (err) {
        if (err) {
            msn.status = 500
            msn.mensagem = "err"
            throw err
        } else {
            msn.status = 200
            msn.mensagem = "Conta Registrada Com sucesso"
            console.log("Conta Registrada Com sucesso");
        }
        res.json(msn)
    })
})

app.post('/login', function (req, res) {
    let usuarios = req.body.todos.split(';')
    let usuariosSeparados = []
    let permitido = false;
    for (let i = 0; i < usuarios.length - 1; i++) {
        let separa = usuarios[i].split(',')
        usuariosSeparados.push(separa)
    }
    for (let i = 0; i < usuariosSeparados.length; i++) {
        if (usuariosSeparados[i][0] == req.body.user && usuariosSeparados[i][1] == req.body.pass) {
            permitido = true;
        }
    }
    //duvida aqui
    if (permitido) {
        const token = jwt.sign({ xxx: req.body.user }, SECRET, { expiresIn: 20})
        return res.json({ auth: true, token})
    } else {
        return res.json({ auth: false})
        res.status(401).end()
    }
})

function verificaUser(req, resp, next) {
    const token = req.header("x-access-token")
    jwt.verify(token, SECRET, function (err, decoded) {
        if (err) {
           console.log("Usuario expirado");
           resp.status(401).end()
        }
        else{
            req.dec = decoded.xxx
            next()
        }
        
        
    })
}
