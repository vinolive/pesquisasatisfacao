var btnEnviar = document.querySelector("#idBtEnviar")

//envia votos à pesquisa.csv
btnEnviar.addEventListener("click",async function(e){
    btnEnviar.disabled = true;
    e.preventDefault();
    var timestamp = new Date().getTime();
    const token = localStorage.getItem('token')
    const update = {
        nmAtendida: document.querySelector("#idAtendida").value,
        nmNota: document.querySelector("#idNota").value,
        nmComportamento: document.querySelector("#idComportamento").value,
        nmProduto: document.querySelector("#idProduto").value,
        nmIdade: document.querySelector("#idIdade").value,
        nmGenero: document.querySelector("#idGenero").value,
        nmTimeStamp: timestamp
      };
      const options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "x-access-token":token
        },
        body: JSON.stringify(update),
      };
      let response = await salvarPesquisa(options);
            if (response.status == 200) {
              document.getElementById("envioAceito").innerHTML=`
              <span style="color:green"> Voto Confirmado </span>
              `
              setTimeout(function () {
                window.location.href="/login";
              }, 2000);
            }else{
              alert("usuario invalido");
            }
})

var btnEstatistica = document.getElementById("idBtnEstatistica");

btnEstatistica.addEventListener("click", async function(e){
    window.location.href="/relatorio"
})

//post do voto pego da página
async function salvarPesquisa(options) {
    let response = await fetch("http://localhost:3000/pesquisa", options);
    return response;
}


