var btnCadastro = document.querySelector("#cadastrar");
btnCadastro.addEventListener("click",async function(e){
    let usuario = document.querySelector("#idUsuario").value;
    let senha = document.querySelector("#idSenha").value;
    let senhaConfirma = document.querySelector("#idSenha2").value;
    let cadastrosAtuais = await retornaUsers();
    let igual=false;
    let igualSenha=false;
    for(let i=0; i<cadastrosAtuais.length; i++){
      if(usuario == cadastrosAtuais[i][0]){
        igual = true;
      }
    }
    if(senha!=senhaConfirma){
        document.querySelector("#retorno").innerHTML="Senhas diferentes"
        setTimeout(function(){
            document.querySelector("#retorno").innerHTML=""
        },1000)
        igualSenha=true;
    }
    if(igual==false&&igualSenha==false){
        document.querySelector("#retorno").style.color = "green";
        document.querySelector("#retorno").innerHTML="Cadastrado Com sucesso"
        setTimeout(function(){
            window.location.href = "/login"
        },1000)

        const update = {
            nmUsuario: usuario,
            nmSenha: senha,
          };
          const options = {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(update),
          };
          let response = await salvarContato(options);
                if (response.status == 200) {
                } else {
                  alert(response.mensagem);
                }
    }else{
      if(igual){
        alert("usuario já cadastrado")
        
      }else{
        alert("senhas nao conferem")
      }
      
    }
})

async function salvarContato(options) {
    let response = await fetch("http://localhost:3000/cadastro", options);
    return response.json();
}

var olho1 = document.querySelector("#olho1")
var olho2 = document.querySelector("#olho2")

olho1.onclick = function(){
  let senha = document.querySelector("#idSenha")
  if(senha.type=="text"){
    senha.type = 'password'
  }else if(senha.type=="password"){
    senha.type="text"
  }
}
olho2.onclick = function(){
  let senha = document.querySelector("#idSenha2")
  if(senha.type=="text"){
    senha.type = 'password'
  }else if(senha.type=="password"){
    senha.type="text"
  }
}
async function retornaUsers() {
  let response = await fetch("http://localhost:3000/usuarios");
  return response.json();
}