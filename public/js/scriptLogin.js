var btnLogin = document.querySelector("#btnLogin")
btnLogin.addEventListener("click", async function () {
    let usuario = document.querySelector("#idUsuario").value;
    let senha = document.querySelector("#idSenha").value;
    let users = await retornaUsers();
    let todos = "";
    for (let i = 0; i < users.length; i++) {
        todos += users[i][0] + ","
        todos += users[i][1] + ";"
    }
    const update = {
        todos: todos,
        user: usuario,
        pass: senha
    };
    const options = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(update),
    };
    let response = await logar(options);
    console.log(response.auth);
    let token = response.token
    localStorage.removeItem("token");
    localStorage.setItem("token", token);
    //duvida aqui
    if (response.auth==false) {
        document.getElementById("respostas").style.color="red";
        document.getElementById("respostas").innerHTML = "Logado inválido";
    } else {
        document.getElementById("respostas").style.color="green";
        document.getElementById("respostas").innerHTML = "Logado com sucesso";
        setTimeout(function () {
            window.location.href = "/";
        }, 1000);
    }
})

async function retornaUsers() {
    let response = await fetch("http://localhost:3000/usuarios");
    return response.json();
}
async function logar(options) {
    let response = await fetch("http://localhost:3000/login", options);
    return response.json();
}