//faz o get que retorna os dados do pesquisa.csv
async function estatisticas() {
    let response = await fetch("http://localhost:3000/estatisticas");
    return response.json();
}

//ao carregar a página busca as estatisticas de avaliação e insere na pagina
window.addEventListener('load', async (event) => {
    x = await estatisticas();
    let texto = `
    <div class="row m-0"> 
        <div class="col bg-light rounded m-2">
                <h3>Solicitação atendida: </h3>
                <p>Não foi atendida: ${Math.round(x.a.nãoFoiAtendida)}%</p>
                <p>Parcialmente Atendida: ${Math.round(x.a.parcialmenteAtendida)}%</p>
                <p>Totalmente Atendida: ${Math.round(x.a.totalmenteAtendida)}%</p>
        </div>
        <div class="col bg-light rounded m-2">
            <h3>Nota do atendimento: </h3>
            <p>Péssimo: ${Math.round(x.b.péssimo)}%</p>
            <p>Ruim: ${Math.round(x.b.ruim)}%</p>
            <p>Aceitável: ${Math.round(x.b.aceitável)}%</p>
            <p>Bom: ${Math.round(x.b.bom)}%</p>
            <p>Excelente: ${Math.round(x.b.excelente)}%</p>
        </div>
	</div>
    <div class="row m-0">
        <div class="col bg-light rounded m-2">
            <h3>Nota do atendente: </h3>
            <p>Indelicado: ${Math.round(x.c.indelicado)}%</p>
            <p>Mal-humorado: ${Math.round(x.c.malHumorado)}%</p>
            <p>Neutro: ${Math.round(x.c.neutro)}%</p>
            <p>Educado: ${Math.round(x.c.educado)}%</p>
            <p>Muito Atencioso: ${Math.round(x.c.muitoAtencioso)}%</p>
        </div>
        <div class="col bg-light rounded m-2">
            <h3>Nota do produto: </h3>
            <div class="d-flex justify-content-around">
            <div><p>0: ${Math.round(x.d.zero)}%</p>
            <p>1: ${Math.round(x.d.um)}%</p>
            <p>2: ${Math.round(x.d.dois)}%</p>
            <p>3: ${Math.round(x.d.tres)}%</p>
            <p>4: ${Math.round(x.d.quatro)}%</p></div>
            <div><p>5: ${Math.round(x.d.cinco)}%</p>
            <p>7: ${Math.round(x.d.sete)}%</p>
            <p>8: ${Math.round(x.d.oito)}%</p>
            <p>9: ${Math.round(x.d.nove)}%</p>
            <p>10: ${Math.round(x.d.dez)}%</p></div>
            </div> 
                
        </div>
    </div>
	<div class="row m-0">
        <div class="col bg-light rounded m-2">
            <h3>Faixa Etária: </h3>
            <div class="d-flex justify-content-around">
            <p>Menos de 15: ${Math.round(x.e.menosDe15)}%</p>
            <p>Entre 15 e 21: ${Math.round(x.e.entre15e21)}%</p>
            <p>Entre 22 e 35: ${Math.round(x.e.entre22e35)}%</p>
        </div>
        <div class="d-flex justify-content-around">
            <p>Entre 36 e 60: ${Math.round(x.e.entre36e50)}%</p>
            <p>Acima de 50: ${Math.round(x.e.acima50)}%</p>
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <button onclick= window.location.href="/login" class="btn btn-danger m-2 w-25" type="button" id="idBtEnviar" value="retorno">Home</button>
    </div>
    `
    document.querySelector("#areaResultado").innerHTML = texto;
});